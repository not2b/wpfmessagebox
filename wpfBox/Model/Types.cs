﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wpfBox
{
	public enum LogLevel
	{
		DEBUG = 0,
		QUESTION = 1,
		INFO = 2,
		WARNING = 3,
		ERROR = 4,
		FATAL = 5
	};

	public enum BoxTypes
	{
		NONE = 0,
		OK = 1,
		OK_CANCEL = 2,
		YES_NO = 3,
		YES_NO_CANCEL = 4,
		ABORT_RETRY = 5,
	};
}
