﻿// using log4net.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using BaseViewModel;

namespace wpfBox
{
	public class MessageViewModel : BaseVM
	{
		const string DEF_ICON_DIRECTORY = @"/wpfBox;component/Resources/"; // @"/assemblyName;component/path"
		const string ERROR_ICON_PATH = DEF_ICON_DIRECTORY + "error_icon.png";
		const string FATAL_ICON_PATH = DEF_ICON_DIRECTORY + "fatal_icon.png";
		const string INFO_ICON_PATH = DEF_ICON_DIRECTORY + "info_icon.png";
		const string WARNING_ICON_PATH = DEF_ICON_DIRECTORY + "warning_icon.png";
		const string QUESTION_ICON_PATH = DEF_ICON_DIRECTORY + "question_icon.png";
		const string DEBUG_ICON_PATH = INFO_ICON_PATH; // to be updated

		#region MEMBERS

		private IList<String> mButtonTexts = new List<String>(4) {"","","",""};
		public IList<String> ButtonTexts
		{
			get
			{
				return mButtonTexts;
			}
			set
			{
				mButtonTexts = value;
			}
		}

		private IList<Visibility> mButtonVisibilityList = new List<Visibility>(4) { Visibility.Visible, Visibility.Collapsed, Visibility.Collapsed, Visibility.Collapsed};
		public IList<Visibility> ButtonVisibilityList
		{
			get
			{
				return mButtonVisibilityList;
			}
			set
			{
				mButtonVisibilityList = value;
			}
		}

		private LogLevel mMessageLevel = LogLevel.DEBUG;
		public LogLevel MessageLevel
		{
			get
			{
				return mMessageLevel;
			}
			set
			{
				mMessageLevel = value;
				switch (mMessageLevel)
				{
					case LogLevel.DEBUG:
						IconPath = DEBUG_ICON_PATH;
						TitleText = "Debug";
						break;
					case LogLevel.QUESTION:
						IconPath = QUESTION_ICON_PATH;
						TitleText = "Input";
						break;
					case LogLevel.INFO:
						IconPath = INFO_ICON_PATH;
						TitleText = "Info";
						break;
					case LogLevel.WARNING:
						IconPath = WARNING_ICON_PATH;
						TitleText = "Warning";
						break;
					case LogLevel.ERROR:
						IconPath = ERROR_ICON_PATH;
						TitleText = "Error";
						break;
					case LogLevel.FATAL:
						IconPath = FATAL_ICON_PATH;
						TitleText = "Fatal Error";
						break;
					default:
						IconPath = DEBUG_ICON_PATH;
						break;
				}
			}
		}

		private string mIconPath = DEBUG_ICON_PATH;
		public string IconPath
		{
			get { return mIconPath; }
			set { mIconPath = value; }
		}

		private string mTitleText;
		public string TitleText
		{
			get { return mTitleText; }
			set { mTitleText = value; }
		}

		private string mMessageText;
		public string MessageText
		{
			get { return mMessageText; }
			set { mMessageText = value;	}
		}

		// private IList<RelayCommand> mButtonCommands = new List<RelayCommand>(4) { };
		public IList<RelayCommand> ButtonCommands
		{
			get;
			private set;
		}

		#endregion MEMBERS

		#region CONSTRUCTORS

		public MessageViewModel(String aMessageText)
		{
			DefaultConstructor(aMessageText, String.Empty, LogLevel.INFO, BoxTypes.OK);
		}

		public MessageViewModel(String aMessageText, LogLevel aLogLevel)
		{
			DefaultConstructor(aMessageText, String.Empty,aLogLevel, BoxTypes.OK);
		}

		public MessageViewModel(String aMessageText, LogLevel aLogLevel, BoxTypes aBoxType)
		{
			DefaultConstructor(aMessageText, String.Empty, aLogLevel, aBoxType);
		}

		private void DefaultConstructor(String aMessageText, String aTitleText, LogLevel aLogLevel, BoxTypes aBoxType)
		{
			this.SetLanguageDictionary();

			MessageText = aMessageText;
			MessageLevel = aLogLevel;

			SetTitle(aTitleText, aLogLevel);
			SetBoxType(aBoxType);
		}

		#endregion CONSTRUCTORS

		private void SetTitle(String aTitleText, LogLevel aLogLevel)
		{
			if(String.IsNullOrEmpty(aTitleText))
			{
				switch (aLogLevel)
				{
					case LogLevel.DEBUG:
						TitleText = "Debug";
						break;
					case LogLevel.QUESTION:
						TitleText = "Input";
						break;
					case LogLevel.INFO:
						TitleText = "Info";
						break;
					case LogLevel.WARNING:
						TitleText = "Warning";
						break;
					case LogLevel.ERROR:
						TitleText = "Error";
						break;
					case LogLevel.FATAL:
						TitleText = "Fatal Error";
						break;
					default:
						TitleText = "Debug";
						break;
				}
			}
			else
			{
				TitleText = aTitleText;
			}			
		}
		private void SetBoxType(BoxTypes aBoxType)
		{
			switch (aBoxType)
			{
				case BoxTypes.OK:
					LoadButtons("Ok");
					break;
				case BoxTypes.OK_CANCEL:
					LoadButtons("Ok", "Cancel");
					break;
				case BoxTypes.YES_NO:
					LoadButtons("Yes", "No");
					break;
				case BoxTypes.YES_NO_CANCEL:
					LoadButtons("Yes", "No", "Cancel");
					break;
				case BoxTypes.ABORT_RETRY:
					LoadButtons("Abort", "Retry");
					break;
				default:
					// TODO Exception
					break;
			}
		}
		private void LoadButtons(String aFirstButtonText = "", String aSecondButtonText = "", String aThirdButtonText = "", String aForthButtonText = "")
		{
			// TODO Look for delegate option
			ButtonCommands = new List<RelayCommand>()
			{
				new RelayCommand(ExecuteFirstAction),
				new RelayCommand(ExecuteSecondAction),
				new RelayCommand(ExecuteThirdAction),
				new RelayCommand(ExecuteForthAction)
			};

			ButtonVisibilityList[0] = String.IsNullOrEmpty(aFirstButtonText) ? Visibility.Collapsed : Visibility.Visible;
			ButtonVisibilityList[1] = String.IsNullOrEmpty(aSecondButtonText) ? Visibility.Collapsed : Visibility.Visible;
			ButtonVisibilityList[2] = String.IsNullOrEmpty(aThirdButtonText) ? Visibility.Collapsed : Visibility.Visible;
			ButtonVisibilityList[3] = String.IsNullOrEmpty(aForthButtonText) ? Visibility.Collapsed : Visibility.Visible;

			ButtonTexts[0] = aFirstButtonText;
			ButtonTexts[1] = aSecondButtonText;
			ButtonTexts[2] = aThirdButtonText;
			ButtonTexts[3] = aForthButtonText;
		}

		private void SetLanguageDictionary()
		{
			//ResourceDictionary dict = new ResourceDictionary();
			//switch (Thread.CurrentThread.CurrentCulture.ToString())
			//{
			//	case "en-US":
			//		dict.Source = new Uri("..\\Resources\\resources.en-GB.xaml", UriKind.Relative);
			//		break;
			//	case "tr-TR":
			//		dict.Source = new Uri("..\\Resources\\resources.tr-TR.xaml", UriKind.Relative);
			//		break;
			//	default:
			//		dict.Source = new Uri("..\\Resources\\resources.en-GB.xaml", UriKind.Relative);
			//		break;
			//}
			//this.Resources.MergedDictionaries.Add(dict);
		}

		private void ExecuteFirstAction()
		{
			//TODO
			Console.WriteLine("First Button Pressed");
		}
		private void ExecuteSecondAction()
		{
			//TODO
			Console.WriteLine("Second Button Pressed");
		}
		private void ExecuteThirdAction()
		{
			//TODO
			Console.WriteLine("Third Button Pressed");
		}
		private void ExecuteForthAction()
		{
			//TODO
			Console.WriteLine("Forth Button Pressed");
		}
		
	}
}